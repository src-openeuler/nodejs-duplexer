%global enable_tests 1
%global module_name duplexer
Name:                nodejs-%{module_name}
Version:             0.1.2
Release:             1
Summary:             Creates a duplex stream
License:             MIT
URL:                 https://github.com/Raynos/duplexer
Source0:             http://registry.npmjs.org/duplexer/-/duplexer-%{version}.tgz
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:       npm(tape) npm(through)
%endif

%description
%{summary}.

%prep
%setup -q -n package
rm -rf node_modules

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/%{module_name}
cp -pr package.json *.js %{buildroot}%{nodejs_sitelib}/%{module_name}
%nodejs_symlink_deps
%if 0%{?enable_tests}

%check
%nodejs_symlink_deps --check
%{__nodejs} -e 'require("./")'
node test
%endif

%files
%doc README.md LICENCE
%{nodejs_sitelib}/%{module_name}

%changelog
* Tue Oct 31 2023 liyanan <liyanan61@h-parners.com> - 0.1.2-1
- Update to 0.1.2

* Tue Aug 18 2020 leiju <leiju4@huawei.com> - 0.1.1-1
- Package init
